package com.tema2.recyclerviewexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Jugador> listado;

    public MyAdapter(Context context, List<Jugador> listado) {
        this.context = context;
        this.listado = listado;
    }

    static class MyHolder extends RecyclerView.ViewHolder {

        private TextView tv_nombre;
        private TextView tv_numero;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            tv_nombre = itemView.findViewById(R.id.tv_jugadores);
            tv_numero = itemView.findViewById(R.id.tv_numero);
        }

        public void setData(String nombre, String numero) {
            tv_nombre.setText(nombre);
            tv_numero.setText(numero);
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.jugadores_holder2, parent, false);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyHolder myHolder = (MyHolder) holder;
        Jugador jugador = listado.get(position);

        String nombre = jugador.getNombre();
        String numero = String.valueOf(jugador.getNumbero());

        // asignamos directamente el nombre al TextView
        //myHolder.tv_nombre.setText(nombre);

        // utilizando el método de la clase MyHolder
        myHolder.setData(nombre, numero);
    }

    @Override
    public int getItemCount() {
        return listado.size();
    }

    public void add(int position, Jugador item) {
        listado.add(position, item);
        notifyItemInserted(position);
    }
    public void add(Jugador item) {
        listado.add(item);
        notifyDataSetChanged();
    }
    public void remove(int position) {
        listado.remove(position);
        notifyItemRemoved(position);
    }
    public void update(List<Jugador> datos) {
        listado.clear();
        listado = datos;
        notifyDataSetChanged();
    }

    public void clearAll() {
        listado.clear();
        notifyDataSetChanged();
    }



}
