package com.tema2.recyclerviewexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapterSegundo extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Jugador> listado;

    public MyAdapterSegundo(Context context, List<Jugador> listado) {
        this.context = context;
        this.listado = listado;
    }

    static class MyHolder extends RecyclerView.ViewHolder {

        private TextView tv_nombre;
        private TextView tv_numero;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            tv_nombre = itemView.findViewById(R.id.tv_jugadores);
            tv_numero = itemView.findViewById(R.id.tv_numero);
        }

        public void setData(String nombre, String numero) {
            tv_nombre.setText(nombre);
            tv_numero.setText(numero);
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (viewType == 2) {
            v = LayoutInflater.from(context).inflate(R.layout.jugadores_holderpar, parent, false);
        } else {
            v = LayoutInflater.from(context).inflate(R.layout.jugadores_holder2, parent, false);
        }
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyHolder myHolder = (MyHolder) holder;
        Jugador jugador = listado.get(position);

        String nombre = jugador.getNombre();
        String numero = String.valueOf(jugador.getNumbero());

        // asignamos directamente el nombre al TextView
        //myHolder.tv_nombre.setText(nombre);

        // utilizando el método de la clase MyHolder
        myHolder.setData(nombre, numero);

        myHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, SegundaActividad.class);
//                context.startActivity(intent);
                Toast.makeText(context, "He pulsado el elemento " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listado.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return 2;
        } else {
            return 1;
        }
    }
}
