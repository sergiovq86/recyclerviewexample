package com.tema2.recyclerviewexample;

public class Jugador {

    private String nombre;
    private int numbero;

    public Jugador(String nombre, int numbero) {
        this.nombre = nombre;
        this.numbero = numbero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumbero() {
        return numbero;
    }

    public void setNumbero(int numbero) {
        this.numbero = numbero;
    }
}
