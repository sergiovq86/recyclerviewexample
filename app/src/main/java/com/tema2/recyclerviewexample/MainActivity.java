package com.tema2.recyclerviewexample;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Jugador> jugadores = new ArrayList<>();
        Jugador jugador = new Jugador("Pepe", 1);
        Jugador jugador1 = new Jugador("Pepe", 2);
        Jugador jugador2 = new Jugador("Pepe", 3);
        Jugador jugador3 = new Jugador("Pepe", 4);

        jugadores.add(jugador);
        jugadores.add(jugador1);
        jugadores.add(jugador2);
        jugadores.add(jugador3);
        jugadores.add(jugador);
        jugadores.add(jugador1);
        jugadores.add(jugador2);
        jugadores.add(jugador3);
        jugadores.add(jugador);
        jugadores.add(jugador1);
        jugadores.add(jugador2);
        jugadores.add(jugador3);
        jugadores.add(jugador);
        jugadores.add(jugador1);
        jugadores.add(jugador2);
        jugadores.add(jugador3);



//        jugadores.add("Pepe");
//        jugadores.add("Antonio");
//        jugadores.add("Jairzinho");
//        jugadores.add("Emerson");
//
//        jugadores.add("Pepe");
//        jugadores.add("Antonio");
//        jugadores.add("Jairzinho");
//        jugadores.add("Emerson");
//
//        jugadores.add("Pepe");
//        jugadores.add("Antonio");
//        jugadores.add("Jairzinho");
//        jugadores.add("Emerson");
//
//        jugadores.add("Pepe");
//        jugadores.add("Antonio");
//        jugadores.add("Jairzinho");
//        jugadores.add("Emerson");

        RecyclerView recyclerView = findViewById(R.id.recyclerview);

        LinearLayoutManager llm = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(llm);

        Drawable drawable = ContextCompat.getDrawable(MainActivity.this, R.drawable.divisor);
        DividerItemDecoration divider = new DividerItemDecoration(MainActivity.this, llm.getOrientation());
        divider.setDrawable(drawable);
        recyclerView.addItemDecoration(divider);

        //        GridLayoutManager glm = new GridLayoutManager(this, 3);
//       // recyclerView.setLayoutManager(glm);
//
//        StaggeredGridLayoutManager sglm = new StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL);
//        recyclerView.setLayoutManager(sglm);

        MyAdapter adapter = new MyAdapter(MainActivity.this, jugadores);
        recyclerView.setAdapter(adapter);

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Jugador jugador = new Jugador("Alfonso", 25);
                adapter.add(jugadores.size() , jugador);
                //adapter.remove(0);
//                List<Jugador> jugadores = new ArrayList<>();
//                Jugador jugador4 = new Jugador("Juan", 3);
//                Jugador jugador5 = new Jugador("Juan", 4);
//                Jugador jugador6 = new Jugador("Juan", 5);
//                Jugador jugador7 = new Jugador("Juan", 6);
//
//                jugadores.add(jugador4);
//                jugadores.add(jugador5);
//                jugadores.add(jugador6);
//                jugadores.add(jugador7);
//                adapter.update(jugadores);

                recyclerView.scrollToPosition(jugadores.size() - 1);
               // adapter.clearAll();
            }
        });

        adapter.notifyDataSetChanged();
    }
}